<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>createBooking</name>
   <tag></tag>
   <elementGuidId>8972da05-ba55-41d2-854c-c84d784904fd</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;firstname\&quot; : \&quot;${firstname}\&quot;,\n    \&quot;lastname\&quot; : \&quot;${lastname}\&quot;,\n    \&quot;totalprice\&quot; : ${totalprice},\n    \&quot;depositpaid\&quot; : \&quot;${depositpaid}\&quot;,\n    \&quot;bookingdates\&quot; : {\n        \&quot;checkin\&quot; : \&quot;${checkin}\&quot;,\n        \&quot;checkout\&quot; : \&quot;${checkout}\&quot;\n    },\n    \&quot;additionalneeds\&quot; : \&quot;${additionalneeds}\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>b9ceaada-3c6b-4dbf-b889-a5c83511effe</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>54c04014-986e-4a6b-8e51-578a9b78048c</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.baseUrl}/booking</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'Monica'</defaultValue>
      <description></description>
      <id>8ee2edae-4b74-4670-bd8d-437aa60293c4</id>
      <masked>false</masked>
      <name>firstname</name>
   </variables>
   <variables>
      <defaultValue>'Cindy'</defaultValue>
      <description></description>
      <id>8994f1ff-d275-4993-84ce-7eee746f71be</id>
      <masked>false</masked>
      <name>lastname</name>
   </variables>
   <variables>
      <defaultValue>111</defaultValue>
      <description></description>
      <id>865b7633-99fc-4b60-8ba8-c579eeffb8cd</id>
      <masked>false</masked>
      <name>totalprice</name>
   </variables>
   <variables>
      <defaultValue>true</defaultValue>
      <description></description>
      <id>1d554119-8668-4b9a-b247-d2d7c7ada57d</id>
      <masked>false</masked>
      <name>depositpaid</name>
   </variables>
   <variables>
      <defaultValue>'2018-01-01'</defaultValue>
      <description></description>
      <id>d43e2172-5829-42fc-98d7-8c0bacb14dd3</id>
      <masked>false</masked>
      <name>checkin</name>
   </variables>
   <variables>
      <defaultValue>'2019-01-01'</defaultValue>
      <description></description>
      <id>c5f427e9-ba89-4225-bd9b-4a817d35fcde</id>
      <masked>false</masked>
      <name>checkout</name>
   </variables>
   <variables>
      <defaultValue>'Breakfast'</defaultValue>
      <description></description>
      <id>d613f52e-2516-4020-aac0-83e859b070f1</id>
      <masked>false</masked>
      <name>additionalneeds</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

def variables = request.getVariables()
def firstname = variables.get('firstname')
def lastname = variables.get('lastname')
def totalprice = variables.get('totalprice')
def depositpaid = variables.get('depositpaid')
def checkin = variables.get('checkin')
def checkout = variables.get('checkout')
def additionalneeds = variables.get('additionalneeds')

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyElementPropertyValue(response, 'booking.firstname', firstname)
WS.verifyElementPropertyValue(response, 'booking.lastname', lastname)
WS.verifyElementPropertyValue(response, 'booking.totalprice', totalprice)
WS.verifyElementPropertyValue(response, 'booking.depositpaid', depositpaid)
WS.verifyElementPropertyValue(response, 'booking.bookingdates.checkin', checkin)
WS.verifyElementPropertyValue(response, 'booking.bookingdates.checkout', checkout)
WS.verifyElementPropertyValue(response, 'booking.additionalneeds', additionalneeds)
assertThat(response.getStatusCode()).isEqualTo(200)

GlobalVariable.bookingId = WS.getElementPropertyValue(response, 'bookingid')
GlobalVariable.firstname = WS.getElementPropertyValue(response, 'booking.firstname')
GlobalVariable.lastname = WS.getElementPropertyValue(response, 'booking.lastname')
GlobalVariable.totalprice = WS.getElementPropertyValue(response, 'booking.totalprice')
GlobalVariable.depositpaid = WS.getElementPropertyValue(response, 'booking.depositpaid')
GlobalVariable.checkin = WS.getElementPropertyValue(response, 'booking.bookingdates.checkin')
GlobalVariable.checkout = WS.getElementPropertyValue(response, 'booking.bookingdates.checkout')
GlobalVariable.additionalneeds = WS.getElementPropertyValue(response, 'booking.additionalneeds')
System.out.println(GlobalVariable.bookingId)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
